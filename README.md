TeamCity one-time passwords plugin
==================

The plugin allows you to log into your TeamCity via one-time passwords.

Install
-------
To install the plugin, put zip archive to `plugins` dir under TeamCity data directory and restart the server.

What is a one-time password
-----
A one-time password (OTP) is a password that is valid for only one login session. OTPs avoid a number of shortcomings that are associated with traditional (static) password based authentication.

The most important advantage of OTPs is that, unlike static passwords, they are not vulnerable to replay attacks. This means that a potential intruder who manages to record an OTP that was already used to log into a service or to conduct a transaction will not be able to abuse it, since it will be no longer valid. The second major advantage is that a user who uses the same (or similar) password for multiple systems, is not made vulnerable in all of them, if the password for one of these is gained by an attacker. 

How to get a one-time password
-----
TeamCity users can get a one-time password using Google Authenticator application. Google Authenticator is an application that implements TOTP security tokens from [RFC 6238](http://tools.ietf.org/html/rfc6238) in mobile apps. Authenticator provides a six- to eight-digit one-time password which users must provide in addition to their username to log into TeamCity.

Typically, users install the Authenticator app on their smartphone ([App Store](https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8), [Google Play](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en)). To log into TeamCity, they run the Authenticator app which produces a six-digit one-time password and provide user name and generated password to the site. TeamCity checks it and authenticates the user.

For this to work, a prior set-up operation is required: TeamCity provides a secret key to the user over a secure channel, to be stored in the Authenticator app. This secret key will be used for all future logins to the site.

How to generate secret key
-----
TeamCity users can generate their secret key in user's profile by clicking the `Generate Secret Key` button below *TeamCity> User Profile> One-time passwords* tab. After that the users should scan QR barcode with the application on their smart phones or enter the secret key manually.

How to log into TeamCity using a one-time password
-----
Use the `Log in using a one-time password` link available on the Login page to log in via your one-time password. Enter your TeamCity username and one-time password, and click the `Log in` button.
  
Configuring TeamCity server
-----
Add the 'One-time Passwords' credentials authentication module to your authentication configuration. Authentication is [configured](https://confluence.jetbrains.com/display/TCD9/Configuring+Authentication+Settings) on the *Administration> Authentication page*.

Build
-----
Run the `mvn package` command from the root project to build the plugin. The resulting package `teamcity-otp-auth-plugin.zip` will be placed in the `target` directory.