package com.stiltsoft.teamcity.auth.otp;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import jetbrains.buildServer.users.SUser;
import jetbrains.buildServer.users.UserModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OtpManagerTest {

    OtpManager otpManager;
    UserModel userModel;
    SUser user;
    String username;

    @Before
    public void setUp() throws Exception {
        username = "admin";
        user = new SUserMock();

        userModel = mock(UserModel.class);
        when(userModel.findUserAccount(null, username)).thenReturn(user);

        otpManager = new OtpManager(userModel);
    }

    @Test
    public void testGenerateAuthKey() throws Exception {
        otpManager.generateSecretKey(username);

        String secretKey = otpManager.getSecretKey(username);
        assertNotNull(secretKey);
    }

    @Test
    public void testAuthorize() throws Exception {
        String secret = "TSEUZYCIKDJH725S";

        int code = 691630;

        GoogleAuthenticator auth = new GoogleAuthenticator();
        assertTrue(auth.authorize(secret, code));
    }

}
