package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.groups.UserGroup;
import jetbrains.buildServer.notification.DuplicateNotificationRuleException;
import jetbrains.buildServer.notification.NotificationRule;
import jetbrains.buildServer.notification.NotificationRulesHolder;
import jetbrains.buildServer.notification.WatchedBuilds;
import jetbrains.buildServer.serverSide.SBuildType;
import jetbrains.buildServer.serverSide.SProject;
import jetbrains.buildServer.serverSide.auth.*;
import jetbrains.buildServer.users.*;
import jetbrains.buildServer.vcs.SVcsModification;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class SUserMock implements SUser {

    private Map<PropertyKey, String> properties;

    public SUserMock() {
        properties = new HashMap<PropertyKey, String>();
    }

    @NotNull
    public List<SVcsModification> getVcsModifications(int numberOfActiveDays) {
        return null;
    }

    @NotNull
    public List<SVcsModification> getAllModifications() {
        return null;
    }

    public void updateUserAccount(@NotNull String username, String name, String email) throws UserNotFoundException, DuplicateUserAccountException, EmptyUsernameException {

    }

    public void setUserProperties(@NotNull Map<? extends PropertyKey, String> properties) throws UserNotFoundException {

    }

    public void setUserProperty(@NotNull PropertyKey propertyKey, String value) throws UserNotFoundException {
        properties.put(propertyKey, value);
    }

    public void deleteUserProperty(@NotNull PropertyKey propertyKey) throws UserNotFoundException {

    }

    public void setPassword(String password) throws UserNotFoundException {

    }

    public void setProjectsOrder(@NotNull List<String> projectsOrder) throws UserNotFoundException {

    }

    public void setVisibleProjects(@NotNull Collection<String> visibleProjects) throws UserNotFoundException {

    }

    public void hideProject(@NotNull String projectId) throws UserNotFoundException {

    }

    public void setLastLoginTimestamp(@NotNull Date timestamp) throws UserNotFoundException {

    }

    public void setBlockState(String blockType, String blockState) {

    }

    @Nullable
    public String getBlockState(String blockType) {
        return null;
    }

    @NotNull
    public List<UserGroup> getUserGroups() {
        return null;
    }

    @NotNull
    public List<UserGroup> getAllUserGroups() {
        return null;
    }

    @NotNull
    public List<VcsUsernamePropertyKey> getVcsUsernameProperties() {
        return null;
    }

    @NotNull
    public List<SBuildType> getOrderedBuildTypes(@Nullable SProject project) {
        return null;
    }

    public void setBuildTypesOrder(@NotNull SProject project, @NotNull List<SBuildType> visible, @NotNull List<SBuildType> invisible) {

    }

    public boolean isHighlightRelatedDataInUI() {
        return false;
    }

    public long getId() {
        return 0;
    }

    public String getRealm() {
        return null;
    }

    public String getUsername() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getEmail() {
        return null;
    }

    public String getDescriptiveName() {
        return null;
    }

    public String getExtendedName() {
        return null;
    }

    public Date getLastLoginTimestamp() {
        return null;
    }

    public List<String> getVisibleProjects() {
        return null;
    }

    public List<String> getAllProjects() {
        return null;
    }

    public boolean isPermissionGrantedGlobally(@NotNull Permission permission) {
        return false;
    }

    @NotNull
    public Permissions getGlobalPermissions() {
        return null;
    }

    @NotNull
    public Map<String, Permissions> getProjectsPermissions() {
        return null;
    }

    public boolean isPermissionGrantedForProject(@NotNull String projectId, @NotNull Permission permission) {
        return false;
    }

    public boolean isPermissionGrantedForAnyProject(@NotNull Permission permission) {
        return false;
    }

    @NotNull
    public Permissions getPermissionsGrantedForProject(@NotNull String projectId) {
        return null;
    }

    @Nullable
    public User getAssociatedUser() {
        return null;
    }

    @NotNull
    public String describe(boolean b) {
        return null;
    }

    @NotNull
    public List<NotificationRule> getNotificationRules(@NotNull String notifierType) {
        return null;
    }

    public void setNotificationRules(@NotNull String notifierType, @NotNull List<NotificationRule> rules) {

    }

    public void removeRule(long ruleId) {

    }

    public void applyOrder(@NotNull String notifierType, @NotNull long[] ruleIds) {

    }

    public long addNewRule(@NotNull String notifierType, @NotNull NotificationRule rule) throws DuplicateNotificationRuleException {
        return 0;
    }

    @Nullable
    public Collection<Long> findConflictingRules(@NotNull String notifierType, @NotNull WatchedBuilds watch) {
        return null;
    }

    @Nullable
    public NotificationRule findRuleById(long ruleId) {
        return null;
    }

    public List<NotificationRulesHolder> getParentRulesHolders() {
        return null;
    }

    @Nullable
    public String getPropertyValue(PropertyKey propertyKey) {
        return null;
    }

    public boolean getBooleanProperty(PropertyKey propertyKey) {
        return false;
    }

    @NotNull
    public Map<PropertyKey, String> getProperties() {
        return properties;
    }

    @NotNull
    public Collection<Role> getRolesWithScope(@NotNull RoleScope scope) {
        return null;
    }

    public Collection<RoleScope> getScopes() {
        return null;
    }

    @NotNull
    public Collection<RoleEntry> getRoles() {
        return null;
    }

    public void addRole(@NotNull RoleScope scope, @NotNull Role role) {

    }

    public void removeRole(@NotNull RoleScope scope, @NotNull Role role) {

    }

    public void removeRole(@NotNull Role role) {

    }

    public void removeRoles(@NotNull RoleScope scope) {

    }

    public boolean isSystemAdministratorRoleGranted() {
        return false;
    }

    public boolean isSystemAdministratorRoleGrantedDirectly() {
        return false;
    }

    public boolean isSystemAdministratorRoleInherited() {
        return false;
    }

    @NotNull
    public Collection<RolesHolder> getParentHolders() {
        return null;
    }

    @NotNull
    public Collection<RolesHolder> getAllParentHolders() {
        return null;
    }
}
