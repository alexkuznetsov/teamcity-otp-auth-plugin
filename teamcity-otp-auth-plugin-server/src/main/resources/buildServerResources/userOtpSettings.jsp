<%@ page import="com.stiltsoft.teamcity.auth.otp.OtpManager" %>
<%@ page import="com.warrenstrange.googleauth.GoogleAuthenticatorKey" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="server" type="jetbrains.buildServer.serverSide.SBuildServer" scope="request"/>
<jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"/>

<style type="text/css">
    .info-message {
        margin-bottom: 20px;
    }
</style>

<%
    String secret = currentUser.getProperties().get(OtpManager.OTP_SECRET_KEY);
    pageContext.setAttribute("secret", secret);
    if (secret != null && !secret.isEmpty()) {
        pageContext.setAttribute("QRBarcodeURL", GoogleAuthenticatorKey.getQRBarcodeURL(currentUser.getUsername(), "TeamCity", secret));
    }
%>
<c:set var="otpSecretKey">${pageScope.secret}</c:set>

<div class="info-message">
    <c:if test="${empty otpSecretKey}">
        One-time passwords allow you to securely log into your TeamCity. To start using one-time passwords you need to generate a secret key.
    </c:if>

    <c:if test="${not empty otpSecretKey}">
        <p>One-time passwords allow you to securely log into your TeamCity. You can get a one-time password using Google Authenticator application (<a href="https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8">App Store</a>, <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en">Google Play</a>). It requires a secret key which you can insert manually or by scanning the QR code below.</p>
        <p>Your secret key is <strong>${otpSecretKey}</strong>.</p>

        <p>
            <img src="${pageScope.QRBarcodeURL}">
        </p>

        <p>If you want to generate a new secret key, click the button below.</p>
    </c:if>
</div>

<div>
    <form id="otp-user-settings" method="post" action="/otp-user-settings.html">
        <input type="hidden" id="currentUser" name="currentUser" value="${currentUser.username}"/>
        <a id="generate-key" class="btn" href="#">
            Generate Secret Key
        </a>
    </form>
</div>

<script type="text/javascript">
    $j(document).ready(function ($) {
        $('#generate-key').click(function() {
            $('#otp-user-settings').submit();
        });
    });
</script>
