<%@ include file="/include-internal.jsp" %>

<bs:externalPage>
    <jsp:attribute name="page_title">Log in using a one-time password</jsp:attribute>
    <jsp:attribute name="head_include">
        <bs:linkCSS>
            /css/forms.css
            /css/initialPages.css
        </bs:linkCSS>
        <bs:linkScript>
            /js/bs/forms.js
            /js/bs/login.js
        </bs:linkScript>
        <script>
            document.documentElement.className = 'ua-js';
        </script>
        <script type="text/javascript">
            $j(document).ready(function ($) {
                var loginForm = $('.loginForm');

                $("#username").focus();

                loginForm.attr('action', '<c:url value='/loginSubmit.html'/>');
                loginForm.submit(function() {
                    return BS.LoginForm.submitLogin();
                });

            });
        </script>
    </jsp:attribute>
  <jsp:attribute name="body_include">
    <div id="loginPage" class="initialPage">
        <span class="logo"><img src="img/logoLogin.png" alt="TeamCity" width="61" height="61"/></span>

        <div id="pageContent">
            <h1 id="header">Log in using a one-time password</h1>

            <div id="errorMessage"></div>

            <form class="loginForm" method="post">
                <table>
                    <tr class="formField">
                        <th><label for="username">Username: <l:star/></label></th>
                        <td>
                            <input class="text" id="username" type="text" name="username"/>
                        </td>
                    </tr>
                    <tr class="formField">
                        <th><label for="password">One-time Password: <l:star/></label></th>
                        <td><input class="text" id="password" type="password" name="password" maxlength="80">
                            <span class="error" id="errorPassword" style="margin-left: 0;"></span>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input class="btn loginButton" type="submit" name="submitLogin" value="Log in">
                            <a class="loginButton" href="<c:url value='/login.html'/>">Cancel</a>
                        </td>
                    </tr>
                </table>

                <input type="hidden" id="publicKey" name="publicKey" value="${publicKey}"/>
            </form>
        </div>
    </div>
  </jsp:attribute>
</bs:externalPage>
