<%@ page import="com.stiltsoft.teamcity.auth.otp.LoginViaOtpController" %>
<%@ include file="/include-internal.jsp"%>

<c:set var="otpPath"><%=LoginViaOtpController.LOGIN_PATH%></c:set>

<p class="loginViaOtp" style="margin: 0; font-weight: normal">
    <span class="greyNote">
        <a href="<c:url value='${otpPath}'/>">Log in using a one-time password</a>
    </span>
</p>