package com.stiltsoft.teamcity.auth.otp;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import jetbrains.buildServer.users.PropertyKey;
import jetbrains.buildServer.users.SUser;
import jetbrains.buildServer.users.SimplePropertyKey;
import jetbrains.buildServer.users.UserModel;

public class OtpManager {

    public static final PropertyKey OTP_SECRET_KEY = new SimplePropertyKey("OTP_SECRET_KEY");

    private UserModel userModel;

    public OtpManager(UserModel userModel) {
        this.userModel = userModel;
    }

    public void generateSecretKey(String username) {
        SUser user = userModel.findUserAccount(null, username);

        if (user != null) {
            GoogleAuthenticator auth = new GoogleAuthenticator();
            GoogleAuthenticatorKey authKey = auth.createCredentials();

            user.setUserProperty(OTP_SECRET_KEY, authKey.getKey());
        }
    }

    public String getSecretKey(String username) {
        SUser user = userModel.findUserAccount(null, username);

        if (user != null) {
            return user.getProperties().get(OTP_SECRET_KEY);
        }

        return null;
    }

    public boolean authorize(String username, Integer code) {
        String secret = getSecretKey(username);

        if (secret != null) {
            GoogleAuthenticator auth = new GoogleAuthenticator();
            return auth.authorize(secret, code);
        }

        return false;
    }
}
