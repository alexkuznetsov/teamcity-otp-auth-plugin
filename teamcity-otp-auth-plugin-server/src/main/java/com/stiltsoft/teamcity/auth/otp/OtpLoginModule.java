package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.serverSide.auth.ServerPrincipal;
import jetbrains.buildServer.serverSide.auth.TeamCityFailedLoginException;
import jetbrains.buildServer.serverSide.auth.TeamCityLoginException;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.util.Map;

import static org.apache.commons.lang.math.NumberUtils.toInt;

public class OtpLoginModule implements LoginModule {

    private Subject mySubject;
    private CallbackHandler handler;
    private Callback[] callbacks;
    private NameCallback nameCallback;
    private PasswordCallback passwordCallback;
    private Map<String, ?> options;

    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        handler = callbackHandler;

        nameCallback = new NameCallback("login:");
        passwordCallback = new PasswordCallback("password:", false);
        callbacks = new Callback[] {nameCallback, passwordCallback};

        mySubject = subject;
        this.options = options;
    }

    public boolean login() throws LoginException {
        try {
            handler.handle(callbacks);
        }
        catch (Throwable t) {
            throw new TeamCityLoginException(t);
        }

        final String login = nameCallback.getName();
        final String password = new String(passwordCallback.getPassword());

        if (checkPassword(login, password)) {
            mySubject.getPrincipals().add(new ServerPrincipal(null, login));
            return true;
        }

        throw new TeamCityFailedLoginException();
    }

    public boolean checkPassword(String username, String password) {
        OtpManager otpManager = (OtpManager) options.get("otpManager");
        return otpManager.authorize(username, toInt(password));
    }

    public boolean commit() throws LoginException {
        return true;
    }

    public boolean abort() throws LoginException {
        return true;
    }

    public boolean logout() throws LoginException {
        return true;
    }
}
