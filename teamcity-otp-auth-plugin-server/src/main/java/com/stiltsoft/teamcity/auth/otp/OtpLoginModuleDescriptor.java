package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.serverSide.auth.LoginConfiguration;
import jetbrains.buildServer.serverSide.auth.LoginModuleDescriptorAdapter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.security.auth.spi.LoginModule;
import java.util.HashMap;
import java.util.Map;

public class OtpLoginModuleDescriptor extends LoginModuleDescriptorAdapter {

    private OtpManager otpManager;

    public OtpLoginModuleDescriptor(LoginConfiguration loginConfiguration, OtpManager otpManager) {
        loginConfiguration.registerAuthModuleType(this);
        this.otpManager = otpManager;
    }

    @Nullable
    @Override
    public String getTextForLoginPage() {
        return "Authenticate to your TeamCity via one-time passwords.";
    }

    public Class<? extends LoginModule> getLoginModuleClass() {
        return OtpLoginModule.class;
    }

    @NotNull
    public String getName() {
        return "OTP";
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "One-time Passwords";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Authentication with one-time passwords generated using your smart phones";
    }

    @Nullable
    @Override
    public Map<String, ?> getOptions() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("otpManager", otpManager);
        return options;
    }
}
