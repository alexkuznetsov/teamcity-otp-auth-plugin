package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.controllers.AuthorizationInterceptor;
import jetbrains.buildServer.controllers.BaseController;
import jetbrains.buildServer.serverSide.crypt.RSACipher;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import jetbrains.buildServer.web.openapi.WebControllerManager;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginViaOtpController extends BaseController {

    public static final String LOGIN_PATH = "/otpLogin.html";

    private PluginDescriptor descriptor;

    public LoginViaOtpController(WebControllerManager webManager,
                                 AuthorizationInterceptor authInterceptor,
                                 PluginDescriptor descriptor) {

        this.descriptor = descriptor;

        webManager.registerController(LOGIN_PATH, this);
        authInterceptor.addPathNotRequiringAuth(LOGIN_PATH);
    }

    @Nullable
    @Override
    protected ModelAndView doHandle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView(descriptor.getPluginResourcesPath("loginPage.jsp"));
        modelAndView.addObject("publicKey", RSACipher.getHexEncodedPublicKey());
        return modelAndView;
    }
}
