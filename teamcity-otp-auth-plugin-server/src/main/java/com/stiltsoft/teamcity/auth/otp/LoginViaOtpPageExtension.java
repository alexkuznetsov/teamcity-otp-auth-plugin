package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.web.openapi.PagePlaces;
import jetbrains.buildServer.web.openapi.PlaceId;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import jetbrains.buildServer.web.openapi.SimplePageExtension;

public class LoginViaOtpPageExtension extends SimplePageExtension {

    public LoginViaOtpPageExtension(PagePlaces pagePlaces, PluginDescriptor pluginDescriptor) {
        super(pagePlaces, PlaceId.LOGIN_PAGE, "loginWithOTP", pluginDescriptor.getPluginResourcesPath("loginViaOtp.jsp"));
        register();
    }
}
