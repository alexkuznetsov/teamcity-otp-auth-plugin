package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.controllers.BaseController;
import jetbrains.buildServer.serverSide.SBuildServer;
import jetbrains.buildServer.web.openapi.WebControllerManager;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OtpUserProfileController extends BaseController {

    private OtpManager otpManager;

    public OtpUserProfileController(SBuildServer server, WebControllerManager webControllerManager, OtpManager otpManager) {
        super(server);
        webControllerManager.registerController("/otp-user-settings.html", this);
        this.otpManager = otpManager;
    }

    @Nullable
    @Override
    protected ModelAndView doHandle(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String currentUser = request.getParameter("currentUser");

        if (currentUser != null) {
            otpManager.generateSecretKey(currentUser);
        }

        return redirectTo("/profile.html?tab=userOtpSettings", response);
    }
}
