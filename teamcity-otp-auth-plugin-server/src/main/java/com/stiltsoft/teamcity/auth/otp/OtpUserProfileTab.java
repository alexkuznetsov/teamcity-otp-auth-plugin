package com.stiltsoft.teamcity.auth.otp;

import jetbrains.buildServer.serverSide.auth.SecurityContext;
import jetbrains.buildServer.web.openapi.PagePlaces;
import jetbrains.buildServer.web.openapi.PlaceId;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import jetbrains.buildServer.web.openapi.SimpleCustomTab;

public class OtpUserProfileTab extends SimpleCustomTab {

    public OtpUserProfileTab(PagePlaces pagePlaces, PluginDescriptor descriptor, SecurityContext securityContext) {
        super(pagePlaces, PlaceId.MY_TOOLS_TABS, "userOtpSettings", descriptor.getPluginResourcesPath("userOtpSettings.jsp"), "One Time Password");
        register();
    }
}
